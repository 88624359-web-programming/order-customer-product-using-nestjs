import { IsNotEmpty, Length } from 'class-validator';
import { IsPositive } from 'class-validator/types/decorator/decorators';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(1, 30)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
