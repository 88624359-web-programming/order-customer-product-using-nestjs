import { IsNotEmpty } from 'class-validator';
import { IsPositive } from 'class-validator/types/decorator/decorators';

class CreatedOrderItemDto {
  @IsNotEmpty()
  productId: number;

  @IsNotEmpty()
  @IsPositive()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
