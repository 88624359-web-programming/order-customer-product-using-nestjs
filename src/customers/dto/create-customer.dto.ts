import { IsNotEmpty, Length } from 'class-validator';
import { IsPositive } from 'class-validator/types/decorator/decorators';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(1, 30)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  age: number;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsNotEmpty()
  @Length(1)
  gender: string;
}
